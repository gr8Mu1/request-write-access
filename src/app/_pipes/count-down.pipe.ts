import { Pipe, PipeTransform } from '@angular/core';
import {interval, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Pipe({
  name: 'countDown'
})
export class CountDownPipe implements PipeTransform {

  constructor() {
  }

  transform(countDownEnd: Date|string): Observable<string> {
    return interval(1000).pipe(
      map(() => {
        if (!countDownEnd) {
          return null;
        }
        countDownEnd = new Date(countDownEnd);  // if orginal type was a string
        const diffSeconds = (countDownEnd.getTime() - (new Date()).getTime()) / 1000;
        return `${diffSeconds} s`;
      })
    );
  }

}
