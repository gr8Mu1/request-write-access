export interface WriteAccess {
  /**
   * User who currently possesses write access, null if none.
   */
  writingUser: string;

  /**
   * User who requested write access, null if none and after a new writingUser has been assigned
   */
  requestingUser: string;

  /**
   * Time when write access will be transferred to requestingUser
   */
  timeNextUser: Date;
}
