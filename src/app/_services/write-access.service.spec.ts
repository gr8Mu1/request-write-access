import { TestBed } from '@angular/core/testing';

import { WriteAccessService } from './write-access.service';

describe('WriteAccessService', () => {
  let service: WriteAccessService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WriteAccessService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
