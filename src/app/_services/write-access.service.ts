import {Injectable, OnDestroy} from '@angular/core';
import {interval, Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {retry,  share, startWith, switchMap,  tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {WriteAccess} from '../_models/write-access';

@Injectable({
  providedIn: 'root'
})
export class WriteAccessService implements OnDestroy {

  public writeAccess$: Observable<WriteAccess> = interval(2000).pipe(
    startWith(0),
    switchMap(() => this.http.get<WriteAccess>(`${environment.baseUrl}/task666/write-access/status`)),
    retry(),
    tap(console.log),
    share(),
  );

  constructor(private http: HttpClient) {

  }

  public requestWriteAccess(userName: string): Observable<WriteAccess> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const params = new HttpParams().set('for', userName);
    return this.http.get<WriteAccess>(`${environment.baseUrl}/task666/write-access/request`, {params});
  }

  public cancelWriteAccessRequest(): Observable<WriteAccess> {
    return this.http.get<WriteAccess>(`${environment.baseUrl}/task666/write-access/cancel`);
  }

  public grantWriteAccess(): Observable<WriteAccess> {
    return this.http.get<WriteAccess>(`${environment.baseUrl}/task666/write-access/grant`);
  }

  public rejectRequest(): Observable<WriteAccess> {
    return this.http.get<WriteAccess>(`${environment.baseUrl}/task666/write-access/reject`);
  }

  ngOnDestroy(): void {
  }

}
