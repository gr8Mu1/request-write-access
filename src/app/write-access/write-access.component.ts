import {Component, Input, OnInit} from '@angular/core';
import {WriteAccessService} from '../_services/write-access.service';
import {WriteAccess} from '../_models/write-access';

@Component({
  selector: 'app-write-access',
  templateUrl: './write-access.component.html',
  styleUrls: ['./write-access.component.css']
})
export class WriteAccessComponent implements OnInit {
  @Input() userName: string;
  private requestingWriteAccess: boolean;

  constructor(
    public writeAccessService: WriteAccessService
  ) {
  }

  ngOnInit(): void {
    this.writeAccessService.writeAccess$.subscribe(writeAccess => {
      if (writeAccess.writingUser === this.userName && this.requestingWriteAccess) {
        console.log(`Write access has been granted to you (${this.userName})`);
        this.requestingWriteAccess = false;
      }
      if (!writeAccess.requestingUser && this.requestingWriteAccess) {
        console.log(`Write access has been denied by user: ${writeAccess.writingUser}`);
        this.requestingWriteAccess = false;
      }
    });
  }


  public requestWriteAccess(): void {
    this.writeAccessService.requestWriteAccess(this.userName).subscribe(
      (writeAccess: WriteAccess) => {
        // TODO: show a modal window with the option to cancel this request
        if (this.userName === writeAccess.writingUser) {
          console.log(`Write access granted immediately`);
        }
        else {
          if (this.userName === writeAccess.requestingUser) {
            console.log(`Requesting write access for you (${this.userName}) press cancel to cancel your request`);
            this.requestingWriteAccess = true;
          }
          else {
            console.log(`Request failed for you (${this.userName}) because someone else (${writeAccess.requestingUser}) is already requesting write access.`);
          }
        }

      }
    );
  }

  cancelRequest(): void {
    this.writeAccessService.cancelWriteAccessRequest().subscribe(
      (writeAccess: WriteAccess) => {
        console.log(`You (${this.userName}) cancelled your write access request, writing user stays: ${writeAccess.writingUser}`);
        this.requestingWriteAccess = false;
      }
    );
  }

  grantAccess(): void {
    this.writeAccessService.grantWriteAccess().subscribe(
      (writeAccess: WriteAccess) => {
        console.log(`You (${this.userName}) granted writing access to ${writeAccess.writingUser}`);
      }
    );
  }

  rejectRequest(): void {
    this.writeAccessService.rejectRequest().subscribe(
      (writeAccess: WriteAccess) => {
        console.log(`You (${this.userName}) denied the other user's request. Writing user stays: (${writeAccess.writingUser})`);
      }
    );
  }
}
