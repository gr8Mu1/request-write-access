import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { SBB_ICON_REGISTRY_PROVIDER } from '@sbb-esta/angular-core/icon'; // TODO

import { AppComponent } from './app.component';
import { WriteAccessComponent } from './write-access/write-access.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { CountDownPipe } from './_pipes/count-down.pipe';

@NgModule({
  declarations: [
    AppComponent,
    WriteAccessComponent,
    CountDownPipe
  ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule
    ],
  providers: [
    // SBB_ICON_REGISTRY_PROVIDER, // TODO
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
