import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WriteAccessComponent } from './write-access.component';

describe('WriteAccessComponent', () => {
  let component: WriteAccessComponent;
  let fixture: ComponentFixture<WriteAccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WriteAccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WriteAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
